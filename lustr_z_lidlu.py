#!/usr/bin/env python3

import time
import pigpio

# pin/konektor/nožička na kterým je přidělanej vysílač
PIN = 21

# pigpio se připojí na todleto pi (jde to i zdáleně/z pc)
pi = pigpio.pi()

if not pi.connected:
    print('nejde se pripojit k demonu \'pigpiod\'')
    exit()

pi.set_mode(PIN, pigpio.OUTPUT)

# první byte s id výrobku
id_vyrobku_kod = (0,1,0,1,0,1,0,1)

# dva možný poslední byty
zapinaci_byte = (0,0,0,1,0,1,0,1)
vypinaci_byte = (0,0,0,1,0,0,1,0)

# id kódy lustrů
# sou pozneněný
lustr1_id_kod  = (0,1,1,0,0,1,1,0,  0,1,1,0,0,1,1,0)
lustr2_id_kod  = (1,0,1,0,1,0,1,0,  1,0,1,0,1,0,1,0)


# časy v microsekundách
# tamto první číslo v kometu je změřená doba
# vono de čekat žeto dycky bude nějaký víc hežčí číslo tak sem vzala
# dycky nejvíc nejbliší kulatý/hezký 

short_delay = 425    #426  trvání krátkýho kopečku
long_delay = 1245    #1246 trvání dlouhýho kopečku
rep_delay = 12465    #12463.6 dlouhatánská pauza mezi dvouma zhlukama signálů
pipnuti_delay = 240  #242.2 takový to děsně krátký sinchronizační pípnutí
lustr_delay = 1      # čekání mezi posílanejma signálama dolustrů v sekundách

# pigpio umí posílat celý pulzy najednou nízkoúrovňově takže by se němelo provevoat různý pythoní drhnutí různý

def pulzy_z_kodu(kod):
    
    # signal je list řady pulzů
    signal = []
    # kolikrát se vopakuje signál
    OPAKOVANI = 5
    for r in range(OPAKOVANI):
        
        # pro první vopakování přeskočíme první bit signálu
        for i in kod[1:] if r==0 else kod:
            if i == 0:
                
                #funkce pigpio.pulse má tři argumenty
                # první sou piny/konektroy/nožičky který se na začátku pulzu zapnou
                # druhej sou piny který se na začátku pulzu vypnou
                # noa třetí je doba trvání pulzu v microsekundách
                
                # piny se vybíraj/nastavujou binární maskou
                # zapnutý/vypnutý budou všecky který sou v masce 1
                # když nastavujem jenom jedinej pin tak nám uplně jednoduše stačí
                # postrčit jedničku vo několik nul doleva voperátorem '<<'
                # nakže třeba pro pin==21 se to rovná 1<<21 a to je '1000000000000000000000'
                
                signal.append(pigpio.pulse(1<<PIN, 0, short_delay))
                signal.append(pigpio.pulse(0, 1<<PIN, long_delay))
            else:
                signal.append(pigpio.pulse(1<<PIN, 0, long_delay))
                signal.append(pigpio.pulse(0, 1<<PIN, short_delay))
                
        signal.append(pigpio.pulse(1<<PIN, 0, pipnuti_delay))
        if r != OPAKOVANI - 1:
            signal.append(pigpio.pulse(0, 1<<PIN, rep_delay))
        else:
            # poslední nemusí bejt tak dlouhej
            # de nám jenom vo to nastavit pin na nulu
            signal.append(pigpio.pulse(0, 1<<PIN, 1))
        
    return signal

# pokavaď existujou vodstraníme už existujicí vyrobený vlny
pi.wave_clear()

# vyrobíme vlnu
pi.wave_add_generic(pulzy_z_kodu(id_vyrobku_kod + lustr1_id_kod + zapinaci_byte))

# uložíme si id vyrobený vlny do proměný 
lustr1_zapinaci_wave = pi.wave_create() 

# takle pro vypínání + druhej lustr

pi.wave_add_generic(pulzy_z_kodu(id_vyrobku_kod + lustr1_id_kod + vypinaci_byte))
lustr1_vypinaci_wave = pi.wave_create()

pi.wave_add_generic(pulzy_z_kodu(id_vyrobku_kod + lustr2_id_kod + zapinaci_byte))
lustr2_zapinaci_wave = pi.wave_create()

pi.wave_add_generic(pulzy_z_kodu(id_vyrobku_kod + lustr2_id_kod + vypinaci_byte))
lustr2_vypinaci_wave = pi.wave_create()


# nakonec pošlem signály do lustrů 
while True:
    pi.wave_send_once(lustr1_zapinaci_wave)
    time.sleep(lustr_delay)
    
    # lustr_delay nám to uplně vpoho zajistí ale kdybysme chtěli hnedka rychle posílat další waveform jinam
    # a takový čekání by nám jako přišlo děsně moc dlouhý mužem čekat furt dokolečka ve víc kračích chvilkách 
    # a dycky se koukat jestli se eště vysílá :O ;D 
    # tady to ale neni potřeba ty lustry maj +- sekundovou rekační dobu po zapnutí/vypnutí během který nanic neslišej :O :O 
    # while pi.wave_tx_busy():
    #    time.sleep(0.1)
    
    pi.wave_send_once(lustr1_vypinaci_wave)
    time.sleep(lustr_delay)
    pi.wave_send_once(lustr2_zapinaci_wave)
    time.sleep(lustr_delay)
    pi.wave_send_once(lustr2_vypinaci_wave)
    time.sleep(lustr_delay)
