import requests
from bs4 import BeautifulSoup
import queue

# prolejzá wikipedii a dycky se snaží přejít na první nenaštívenej další vodkaz na nějakej wiki článeček noa ty články jakoby počitá dokavaď nenajde nějakej vo fylozofii nebo už nemá jako kam jít jinam :O ;D

kolikaty_clanek = 1
url = 'https://en.wikipedia.org/wiki/Special:Random'

posledni_navstivene = queue.Queue(
    1000)  # si myslim že je jakože dost nepravděpodobný že budem cyklovat pokaždý ve stejný tisícovce článečků :D ;D

while True:
    # stáhnem stránku z url a uděláme z ní BeautifulSoup oběkt
    stranka = BeautifulSoup(requests.get(url).content, features='lxml')
    titulek = stranka.find('h1', {'id': 'firstHeading'})  # titulek stránky

    # zda je jakoby vo tý fylofozii daná stránka
    # najdem div kde sou uvedený kategorie danýho článku
    kategorie = stranka.find('div', {'id': 'catlinks'})
    for link in kategorie.findAll('a'):
        if 'Category:Philosophy' in link['href']:
            # pokud tam je vodkaz na fylozofii tak jako asi máme hotovo
            print('našli sme fylozfii!!!!!!!!!!!!')
            print(f'ato na {kolikaty_clanek}. článku')
            quit()

    # pokud ne najdem vodkaz a deme dál
    # najdem div s obsahem stránky
    wiki_block = stranka.find('div', {'id': 'mw-content-text'})
    linky = wiki_block.findAll('a')

    # hledáme takovej link aby měl jenom jeden jedinej attribut 'href'
    # vostatní mužou bejt vobrázky titulky nebo něco divnýho
    # pokuď vobsahujou znak ':' tak taky jako zahazujem
    next_url = None
    for link in linky:
        if not link.has_attr('class'):
            if 'wikimedia' in link['href'] or ':' in link['href']:
                # link je špatnej/divnej takže berem další v pořadí
                continue
            # zkusíme sestavit novou url a kouknout jestli sme tam nááhodou už nebyli
            # do fronty naštívenejch
            next_url = 'https://en.wikipedia.org' + link['href']
            if next_url in posledni_navstivene.queue:
                next_url = None
                continue
            # print(link)
            break

    # pokud nenajdem žádnej validní link
    if next_url is None:
        print('neni kam pokracovat!!!!!!! :O :O :\'( :\'(')
        print(f'selhali sme na {kolikaty_clanek}. radku')
        quit()

    print(f'ze stránky s titulkem: {titulek.text} přecházíme na \'{next_url}\'')

    # z fronty musíme zahazovat ručně
    if posledni_navstivene.full():
        posledni_navstivene.get()

    # strčíme naštívenou do fronty
    posledni_navstivene.put(next_url)
    url = next_url
    kolikaty_clanek += 1

print('\'oop je uplně na houby\' řek aristoteles platonusoj ale pppppššššššššš :O :O :D ;D')