// furt stejnejch prvních 8 bitů
#define PRVNI_BYTE 0b01010101

// id lustrů pozněněný
// reálný vypadaj jinak
// prozradim jenom že první má 6 nul a 10 jedniček
// a druhej 7 nul a 9 jedniček aže se asi snažej vo nějaký rozložení
// těch nul/jedniček na časový vose asi :D ;D
#define ID_LUSTRU_1 0b0110011001100110
#define ID_LUSTRU_2 0b1010101010101010

// dva možný poslední končicí byty
#define ZAPINACI_BYTE 0b00010101
#define VYPINACI_BYTE 0b00010010

// různý časy v microsekundách
#define KRATKA_PAUZA 426
#define DLOUHA_PAUZA 1246
#define PAUZA_MEZI_ZHLUKAMA 12463
#define TRVANI_PIPNUTI 242

// nožička arduina na který máme přidělanej vysílač
// 13ka je věčinou taky led světýlko takže mužem vidět jak to
// jako posílá ten signál :D
#define VYSILACI_PIN 13

// funcke který dělaj že pošlou kopeček s mezerou
// který sou buťto jednička nebo nula
void poslatNulu()
{
    digitalWrite(VYSILACI_PIN, HIGH);
    delayMicroseconds(KRATKA_PAUZA);
    digitalWrite(VYSILACI_PIN,LOW);
    delayMicroseconds(DLOUHA_PAUZA);
}

void poslatJednicku()
{
    digitalWrite(VYSILACI_PIN, HIGH);
    delayMicroseconds(DLOUHA_PAUZA);
    digitalWrite(VYSILACI_PIN,LOW);
    delayMicroseconds(KRATKA_PAUZA);
}

//takový to krátký pípnutí uplně nakonci těch 32 bitů
void synchronizacniPipnuti()
{
    digitalWrite(VYSILACI_PIN, HIGH);
    delayMicroseconds(TRVANI_PIPNUTI);
    digitalWrite(VYSILACI_PIN,LOW);
}

void poslatSignal(unsigned int lustrID, bool stav)
{

    for(int opakovani=0; opakovani<5; opakovani++)
    {
        // přeskakujem uplně nejprvnější bit dycky
        for(int i=opakovani?7:6; i>=0; i--)
        {
            if((PRVNI_BYTE >> i) & 0x01)
                poslatJednicku();
            else
                poslatNulu();
        }

        for(int i=15; i>=0; i--)
        {
            if((lustrID >> i) & 0x0001)
                poslatJednicku();
            else
                poslatNulu();
        }

        char koncovyByte = stav?ZAPINACI_BYTE:VYPINACI_BYTE;
        for(int i=7; i>=0; i--)
            if((koncovyByte >> i) & 0x01)
                poslatJednicku();
            else
                poslatNulu();

        synchronizacniPipnuti();
        if(opakovani<4)
            delayMicroseconds(PAUZA_MEZI_ZHLUKAMA);

    }


}


void setup() {
    pinMode(VYSILACI_PIN, OUTPUT);
}

void loop() {
    poslatSignal(ID_LUSTRU_1, true);
    delay(3000);
    poslatSignal(ID_LUSTRU_1, false);
    delay(3000);
    poslatSignal(ID_LUSTRU_2, true);
    delay(3000);
    poslatSignal(ID_LUSTRU_2, false);
    delay(3000);
}
